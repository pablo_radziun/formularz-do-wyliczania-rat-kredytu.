Formularz do wyliczania rat kredytu. Formularz ma zawierać pola:

Wnioskowana kwota kredytu

Ilość rat

Oprocentowanie

Opłata stała

Rodzaj rat : malejąca, stała


Po wysłaniu danych z formularza ma wyświetlić się strona z harmonogramem spłat. Harmonogram spłat jest przedstawiony w postaci tabeli z kolumnami:

Nr raty

Kwota Kapitału

Kwota odsetek

Opłaty stałe

Całkowita kwota raty

Jeśli formularz został nieprawidłowo wypełniony to użytkownik nie może przejść do strony z harmonogramem. Do formularza dodaj dodatkowo przycisk, dzięki któremu uzytkownik będzie mógł sobie wygenerować plik pdf z harmonogramem spłaty.
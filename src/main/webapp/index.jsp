﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator Kredytowy - Pawel Radziun</title>
</head>
<body>
<h1>Formularz do symulacji kredytu</h1></br>
		<form action="hello" method="post">
			<label>Wnioskowana kwota kredytu: <input type="text" name="kwotaKredytu" required /></label><br/></br>
			<label>Ilosc rat: <input type="text" name="iloscRat" required /></label><br/></br>
			<label>Oprocentowanie: <input type="text" name="oprocentowanie" required /></label><br/></br>
			<label>Opłata stala: <input type="text" name="oplataStala" required /></label><br/></br>
			<label>Rodzaj rat: <select name="rodzajRat"><option value="malejaca">Malejaca</option><option value="stala">Stala</option></select></label>
			<input type="submit" value="Wyslij"/><button type="submit" name="pdf" value="pdf">Generuj PDF</button>
		</form>
		</br></br>
		Autor: Pawel Radziun s13407
</body>
</html>
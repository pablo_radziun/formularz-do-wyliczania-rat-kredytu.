﻿package servlets;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		double kwotaKredytu = Double.parseDouble(request.getParameter("kwotaKredytu"));
		int iloscRat = Integer.parseInt(request.getParameter("iloscRat"));
		double oprocentowanie = Double.parseDouble(request.getParameter("oprocentowanie"))/100;
		double oplataStala = Double.parseDouble(request.getParameter("oplataStala"));
		String rodzajRat = request.getParameter("rodzajRat");
		
		if(kwotaKredytu<=0 || iloscRat<=0 || oprocentowanie<0 || oplataStala<0 || rodzajRat==null || rodzajRat.equals("")) {
			response.sendRedirect("/");
		}
		else {
			response.setContentType("text/html");
			StringBuilder sb = new StringBuilder();
			sb.append("<!DOCTYPE html>"
					+ "<html>"
					+ "<head>"
					+ "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>"
					+ "<title>Harmonogram spłaty kredytu</title>"
					+ "</head>"
					+ "<body>");
			sb.append(harmonogramSplaty(kwotaKredytu, iloscRat, oprocentowanie, oplataStala, rodzajRat));
			sb.append("</body>"
					+ "</html>");
			
			if(request.getParameter("pdf") != null) {
				exportToPdf(sb);
				response.sendRedirect("/");
			}
			else {
				response.getWriter().print(sb);
			}
		}
	}

	public StringBuilder harmonogramSplaty(double kwotaKredytu, int iloscRat, double oprocentowanie, double oplataStala, String rodzajRat) {
		
		double kwotaKapitalu = 0, kwotaOdsetek = 0, kwotaRaty = 0;
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("<table border=5>"
				+ "<tr>"
				+ "<th>Nr raty<br/></th>"
				+ "<th>Kwota kapitalu<br/></th>"
				+ "<th>Kwota odsetek<br/></th>"
				+ "<th>Oplaty stale<br/></th>"
				+ "<th>Calkowita kwota raty<br/></th>"
				+ "</tr>");
		
		for (int numerRaty=1; numerRaty<iloscRat+1; numerRaty++){
			
			if (rodzajRat.equals("malejaca")) {
				kwotaKapitalu = kwotaKredytu/iloscRat;
				kwotaOdsetek = kwotaKapitalu*(1+(iloscRat-numerRaty+1)*oprocentowanie/12)-kwotaKapitalu;
				kwotaRaty = kwotaKapitalu + kwotaOdsetek + oplataStala;
			}
			else if (rodzajRat.equals("stala")) {
				kwotaKapitalu = kwotaKredytu/iloscRat;
				kwotaOdsetek = kwotaKredytu*oprocentowanie/12;
				kwotaRaty = kwotaKredytu*Math.pow(1+(oprocentowanie/12), iloscRat)*(1+(oprocentowanie/12)-1)/(Math.pow(1+(oprocentowanie/12),  iloscRat)-1);
			}
			
			sb.append("<tr>"
					+"<td>"+numerRaty+"</td>"
					+"<td>"+new BigDecimal(kwotaKapitalu).setScale(2,RoundingMode.HALF_UP).doubleValue()+"</td>"
					+"<td>"+new BigDecimal(kwotaOdsetek).setScale(2,RoundingMode.HALF_UP).doubleValue()+"</td>"
					+"<td>"+new BigDecimal(oplataStala).setScale(2,RoundingMode.HALF_UP).doubleValue()+"</td>"
					+"<td>"+new BigDecimal(kwotaRaty).setScale(2,RoundingMode.HALF_UP).doubleValue()+"</td>"
					+"</tr>");
			}
			sb.append("</table>");
		
		return sb;
	}
	
	private void exportToPdf(StringBuilder sb) throws IOException {
		
		 Document document = new Document();
		 
		 try {
			 PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream("harmonogram.pdf"));
			 document.open();
			 XMLWorkerHelper.getInstance().parseXHtml(pdfWriter, document, new StringReader(sb.toString()));
		 }
		 catch (DocumentException e) {
			 System.err.println(e.getMessage());
		 }
		 catch (IOException e) {
			 System.err.println(e.getMessage());
		 }
		 
		 document.close();
	}
}